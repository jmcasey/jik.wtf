+++
render = true

[extra]
ogham = "sdflk"
+++
Howdy! Welcome to my homepage (is that too old-fashioned?).

I'm a security engineer with experience across most of the OSI stack. Recently I began dipping my toes into the world of Rust development. I'm passionate about computing and have a hobby of exploring esoteric and fringe systems and programs.

\

---
