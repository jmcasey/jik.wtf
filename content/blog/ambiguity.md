+++
title = "Ambiguity"
date = 2023-02-27
subtitle = "RICHARD HAMMING (YOU AND YOUR RESEARCH, 1986)"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚓ ᚇᚐᚈᚐ ᚈᚏᚓᚅᚋᚇ᚜"
+++

Great scientists tolerate ambiguity very well. They believe the theory enough to go  
ahead; they doubt it enough to notice the errors and faults so they can step forward and  
create the new replacement theory. If you believe too much you’ll never notice the flaws;  
if you doubt too much you won’t get started. It requires a lovely balance.”  