+++
title = "Password Attack Mitigations"
date = 2021-10-09
page_template = "page.html"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚔᚌᚐᚏᚐᚌᚓᚄ ᚔᚄ ᚆᚐᚏᚇ᚜"
+++

Login attempts should be limited to a rate which is reasonable for a normal human to achieve. Any more attempts than (for example) one attempt per second should raise suspicion.

Many systems implement automatic account lockouts after a certain number of failed login attempts (sometimes within a certain time frame). This can lead to the attackers IP being blocklisted.

When attempting to validate credentials, ideally find an API or VPN endpoint which validates credentials instantly and is not rate limited or otherwise monitored.

To avoid Rate Limiting and account lockout policies, another type of password attack is password spraying. Instead of throwing many passwords at one account, the attack sprays the most common passwords against a large number of accounts.

Although it is less targeted (a successfully compromised account may not have the access needed by the attacker), it may be enough of a foothold to progress further into the environment.

In order to properly tune a spray attack to avoid detection, an attacker needs an idea of the specific lockout policies in place on a given system.
