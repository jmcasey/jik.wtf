+++
title = "Bitcoin as Thin Waist for the Blocknet"
date = 2023-02-16
subtitle = "Lessons from TCP/IP"

[extra]
ogham = "᚛ᚃ ᚏᚓᚐᚂᚈᚔᚋᚓ ᚇᚐᚈᚐ ᚈᚏᚓᚅᚈ ᚌᚐᚏᚐᚌᚓᚄ ᚆᚐ᚜"
+++

Many things drove the success of the internet. From a socio-techno perspective, perhaps the largest driver was the Internet Protocol as a "thin waist" for the TCP/IP stack as a whole.

By centralising around a single mid-stack protocol, software above and below were able to target a single platform, knowing that inter-compatibility was guaranteed.

While there are risks associated with putting all of our eggs in a single basket (all attackers will be focused on a single target), should the nascent protocol survive this onslaught then it shall have been forged by fire.

Bitcoin is a decentralised digital currency that offers a secure, tamper-proof, and globally accessible means of storing and transferring value without the need for intermediaries or central authorities.

Just as text and files are foundational building blocks of Unix systems, so too can bitcoin form a foundational "value settlement" layer in a next generation internet stack.


