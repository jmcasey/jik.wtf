+++
title = "Privilege Escalation on Linux"
date = 2022-04-16
page_template = "page.html"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚔᚅᚌ ᚈᚓᚏᚐᚁᚔᚈᚄ ᚑᚃ ᚏᚓᚐᚂᚈᚔᚋᚓ ᚇᚐᚈᚐ ᚈᚑ ᚈᚓᚅ ᚇᚔᚃᚃᚓᚏᚓᚅᚈ ᚌᚐᚏᚐᚌᚓᚄ ᚔᚄ ᚆᚐᚏᚇ᚜"
+++


First step when attempting to elevate permissions is gathering information. You should endevour to discover:

##### Distro type, Kernel version, etc

`cat /etc/issue`
`cat /proc/version`
`hostname`
`uname -a`

##### Running applications and services

`ps aux` - Retrieves information about applications and services.
`ps aux | grep root` to find processes running with root privileges.
`dpkg -l` - Lists installed applications (Debian).
`rpm -qa` - "" "" (Fedora).
`ls -ls /etc/ | grep .conf`
`ls -ls /var/www/html/`

* Useful for analysis of *CMS* machines.
  * [Joomla](../../Joomla.md) - `configuration.php`
  * *Wordpress* - `wp-config.php`

* `find /* -user root -perm -4000 -print 2>/dev/null`
  * Finds all *SUID* programs on a system.
  * !important

##### Scheduled jobs

* *cron*

##### Files

TODO: Revise this

* `cat /etc/fstab` - list unmounted file systems
* World writable directories:
  * `find / \( -wholename '/home/homedir*' -prune \) -o \( -type d -perm -0002 \) -exec ls -ld '{}' ';' 2>/dev/null | grep -v root`
* World writable directories for root:
  * `find / \( -wholename '/home/homedir*' -prune \) -o \( -type d -perm -0002 \) -exec ls -ld '{}' ';' 2>/dev/null | grep root`
* World writable files:
  * `find / \( -wholename '/home/homedir/*' -prune -o -wholename '/proc/*' -prune \) -o \( -type f -perm -0002 \) -exec ls -l '{}' ';' 2>/dev/null`
* World writable files in /etc/:
  * `find /etc -perm -2 -type f 2>/dev/null`
* World writable directories:
  * `find / -writable -type d 2>/dev/null`

##### Networking

* `ifconfig -a`
* `route` -  Shows the routing table.
* `netstat -antup` - Lists active connections to the terminal.
* `arp -e`  - *ARP* table entries.

##### Users

* `cat /etc/passwd`
  * Note: an `x` in the password field means that the encrypted password is in `/etc/shadow`
* `who`
* `w`
* `id`

````output
uid=1000(jack) gid=1000(jack) groups=1000(jack),3(sys),90(network),98(power),108(vboxusers),991(lp),998(wheel)
````

Pay attention to the groups the user belongs to. The sudo group is particularly obviously useful.

* `sudo -l`

````output
Runas and Command-specific defaults for jack:
Defaults!/etc/ctdb/statd-callout !requiretty

User jack may run the following commands on theia:
    (ALL) ALL
````

Requires:

1. A vulnerable kernel
1. A working exploit
1. A way to transfer the exploit code to the target device
1. A way to compile the target
1. A way to execute the exploit 

Generally, systems can be protected against the final three steps of \[kernel exploitation\](Exploiting the Linux Kernel).

3. **Prevent transferring the exploit**
   
   * Disabling unnecessary tools and services that could be used to *live off the land*.
     * FTP, TFTP, SMB, SCP, `wget`, `curl`
   * Allowlisting access to these tools.
   * Create watchers to detect unauthorised usage.
3. **Remove compilation tools**
   
   * Similarly, compilation tools such as `gcc`, `cc`, and other development tools may be removed, allowlisted, and monitored by a *SOC*.
3. **Prevent exploit execution**
   
   * Limiting writable and executable directories on the system.
   * In particular, *world-writable* directories are concerning.
     * `/tmp`
     * `/dev/shm`
   * By creating separate partitions, we can mount directories such as `/tmp` and `/home` on a separated 'noexec' file system. *Placing /home on a separate partition.*
   * `chmod 700 /path/to/file` - restricts `rwx` permissions to the owner of the file.
   * *Exploiting SUID permissions*
     * E.g. Nano with SUID bit allows opening `/etc/passwd` as root [Linux passwd File](../../000%20Zettelkasten/Linux%20passwd%20File.md)

LXD is an *open-source* *container* manager that provides features and functionality to build and manager Linux containers on Linux hosts.

LXD is built on top of LXC, extending it with a *REST* *API*  that is accessible over a local UNIX socket (or network). All tasks are performed through API calls.

LXD may be vulnerable to PrivEsc. Access is based on group membership; if you are part of the `lxd` group it is possible to escalate to `root`. This includes the ability to attach any filesystem paths or devices to an instance

---

See also:

* https://linuxcontainers.org/lxc/introduction/
* https://linuxcontainers.org/lxd/introduction/
* "Mounting the root filesystem in a container" VHL Courseware, pg 231.

#### Automated Scripts

* Linux Privilege Escalation Awesome Script - LinPEAS
* Linux Privilege Escalation Checker - linuxprivchecker.py
* Unix PrivEsc Check - unix-privesc-check

##### 

---

Source: VHL-Penetration-Testing-Courseware-V1.pdf

Further reading:

* https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/
* http://netsec.ws/?p=309

---

Status: #🌻
Tags: 
