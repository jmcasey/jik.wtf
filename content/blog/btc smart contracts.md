+++
title = "Bitcoin Smart Contracts"
date = 2023-02-17
subtitle = "Turns out, no longer impossible!"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚔᚅᚌ ᚈᚓᚏᚐᚁᚔᚈᚄ ᚑᚃ ᚏᚓᚐᚂᚈᚔᚋᚓ ᚇᚐᚈᚐ ᚈᚑ ᚈᚓᚅ ᚇᚔᚃᚃᚓᚏᚓᚅᚈ ᚌᚐᚏᚐᚌᚓᚄ ᚔᚄ ᚆᚐᚏᚇ᚜"
+++

This is a look into Stacks, as a potential smart contract platform which uses the Bitcoin blockchain as a foundation.

Stacks is the brainchild of [[Muneeb Ali]]. The earliest research I have found is in Dr. Ali's thesis: "Trust-to-Trust Design of a New Internet". (which to be fair is a badass title)

Ali's chainet aims to give the end user explicit control over trust decisions and move trust from the core of the network to the edge by removing "trust points" from the middle of the network.

To overcome the current space/time/energy constraints of the Bitcoin blockchain, Ali offers "virtualchains". A virtual blockchain which can be assembled using data from an underlying blockchain.

This enables an interesting hybrid of consensus mechanisms. Since the security of the underlying blockchain remains intact, "history" is secured by PoW. The virtualchain on top can use it's own, independent consensus mechanism.

Further, the doctor presents a "novel Sybil-protection mechanism" for blockchains. Could be PoX?

Surely other folks have tried this? TODO: Research

---

Components:
- Service Naming and Discovery
	-[[ Zooko's Triangle]]
- Data Storage

Design Considerations:
- Separation of the Control and Data Plane
	- Control: Name Registration
	- Data: User/[[Service Discovery]] and Availability
- Agnostic to the Underlying Blockchain
	- Allows migration crosschain
	- Used to deliver totally-ordered operations
- Flexibility to Construct Arbitrary State Machines
	- Futureproof

---

# Blockstacks Overview

![[Pasted image 20230320193632.png]]
- [[Blockchain]]
	- Stores and orders operations by [[consensus]]
- [[Virtualchain]]
	- Layer 2
	- General [[State Machines]]
-TRUST BORDER-
- Discovery
	- This layer is not trusted by the user, who must perform data integrity checks
	- Defines "zone files" which store routing information for locating data bytes
		- Uses DNS file formatting
		- Blockstack defines the [[Atlas network]] for storing zone files
		- A zone file can only be written, if hash(zonefile) was previously announced in the blockchain
			- A data
- Storage
	- Retrieved data is verified using a declared data key.

How is a Zone File Hash calculated?
How is a Zone File retrieved?

Potential DOS: A significant proportion of the BTC blockchain must be stored to fully verify the discovery layer.

So it takes two bitcoin blocks to declare a name?

URI - This is the point at which a protocol like [[atproto]] could interconnect

---

Quick detour to [[Namecoin]]

Registering a name takes two steps

First, the user announces the intention to register a domain by including `hash(name)` in a **pre-order** transaction

At a later time, a second transaction completes the **register** step

A registration expires after 36,000 blocks

---

[[Blockchain Name System]]

Operations
- Pre-order
- Register
- Update
- Transfer
- Revoke

BNS Namespace == DNS TLD
- The cost and renewal rate of names are defined by the [[namespace]] pricing functions 
- Each namespace in BNS gets its own [[blockchain]].

A [[consensus hash]] can be calculated at any blockchain blockheight n:

Consensus Hash (n) = Hash (Vn + Pn)

Where:
- Vn = block n's sequence of virtualchain operations
- Pn = Merkle root of prior consensus hashes
	- => { CH(p)|p = n - 2^i }
		- i is a Natural Number
		- n-2^i >= n0

A trusted [[Consensus Hash]] allows for Simplified Name Verification - a way to resolve a name in log time.

---

Virtual Chain

Layer 2 Challenges
- Failure
	- If bitcoin falls we're all fucked anyway
- Fork
	- Fork-consistent [[replicated state machines]]


---

Block N: Candidates send PoX to TREASURER (Virtual chain state: Need Sigs)
 (if leader rugs, their STX stake is used to return funds)
 
Block N+1: Leader locks multisig wallets which returns the coins
Block N+2: New leader elected deterministically


Candidates send PoX to the Treasurer to become President
A candidate is deterministically elected President

The President publishes multisig locked BTC (president can be harshly punished)

The President can declare transactions
The secretary can sign them
anyone can broadcast

President & Treasurer generate and broadcast the address of the next election
Candidates pre-announce PoX intentions
President signs de-pegging transaction
Candidates peg into the address
Treasurer co-signs the PoX transactions which actually happened

Dishonest candidates have their BTC burned/taken
Dishonest presidents cannot unpeg their BTC
Dishonest treasurers shouldn't exist?

This also moves the leader from one person to n people
Also indirectly ensures that the system cannot "borrow" pegs across "sessions"

address is generated
address is broadcast
FC peg into the address
CP broadcasts an unpeg transaction
CT signs the unpeg transaction (block complete)

FP is duly elected
If honest
	CP becomes FT
if dishonest
	PoX is lost (Treasurer refuses to sign)
	
FP and FT 



---

Follow on research

https://www.cs.princeton.edu/techreports/2017/003.pdf
https://medium.com/@VitalikButerin/quadratic-arithmetic-programs-from-zero-to-hero-f6d558cea649
https://medium.com/rareskills/blockchain-job-tier-list-dd4775cf4bfe
