+++
title = "So you wanna use your domain name as your bluesky handle?"
date = 2023-03-07
page_template = "page.html"

[extra]
ogham = "᚛ᚄᚈᚏᚓᚐᚋᚔᚅᚌ ᚈᚓᚏᚐᚁᚔᚈᚄ ᚑᚃ ᚏᚓᚐᚂᚈᚔᚋᚓ ᚇᚐᚈᚐ ᚈᚑ ᚈᚓᚅ ᚇᚔᚃᚃᚓᚏᚓᚅᚈ ᚌᚐᚏᚐᚌᚓᚄ ᚔᚄ ᚆᚐᚏᚇ᚜"
+++

**Hey! You don't have to do this anymore. There's a handy dandy flow available in the in-app settings.**

If successful, you will immediately give up control of your current \*.bsky.social handle, which may then be available for registration by other people.

### Required

- Bluesky account
- A domain name you purchased through a registrar
- (In the future, alternatively): XRPC server 

### Before You Proceed

This is an unsupported, bleeding-edge feature.

Please only proceed if you feel comfortable with bugs, account access issues, lost data, temporal shifts, and potential space invasion. I do not work for Bluesky PBLLC and this is not legal advice. Actions often have consequences.

Seriously. You'll be able to do this through the app in like a week.

### You Must Determine Your True Name, Eragon

Since the [@-protocol](https://atproto.com/) allows you to change your handle, your account is identified by a DID document, and in turn by its [hash](https://www.w3.org/TR/did-core/#a-simple-example.) To find out what that is, you can send an HTTP request to the handle `update()` method.

Here's a few ways to do that:

#### Using [Postman](https://www.postman.com/):

```
GET: https://bsky.social/xrpc/com.atproto.handle.resolve
- Params:
	- Key: handle
	- Value: your.current.handle
- Authorization: None
- Body: None
```

#### Using cURL
```
curl https://bsky.social/xrpc/com.atproto.handle.resolve?handle=your.current.handle
```

#### Using httpie
```
http get https://bsky.social/xrpc/com.atproto.handle.resolve \
handle==your.current.handle
```

If you get a response of "did:fake:donotuse", then something went wrong. If it looks like gibberish, you're in business.

### Domain Name Server Settings

You need to add a `TXT` record to your domain.

- Subdomain: \_atproto
- Value: did=did:plc:\<your-did-hash-here\>
- TTL: Default or 3600

Mine looks like this:

![dns configuration screenshot](/_atproto.jpg)

## 

### Authenticate with bsky.social PDS

#### Using [Postman](https://www.postman.com/):

```
GET: https://bsky.social/xrpc/com.atproto.session.create
- Params:
	- Key: handle
	- Value: your.current.handle
- Authorization: None
- Body:

{
    "identifier": "your.current.handle",
    "password": "password"
}

```

We need the `accessJwt` token from the reponse. Be careful to not copy any quotes or whitespace.

Paste the token into Authorization > Type: Bearer Token.

#### Using cURL
```

```

#### Using httpie
```
http post https://bsky.social/xrpc/com.atproto.session.create identifier=your.current.handle password=<your-password>
```

### Request Handle Rotation


```
POST: https://bsky.social/xrpc/com.atproto.handle.update

- Authorization: Bearer Token = <accessJwt>
- Params: None
- Body:
	- Encoding: application/json (click the dropdown menu)

{
    "handle": "your.domain.handle",
}

```


Using `cURL`:
```

```

Using `httpie`:
```
http post https://bsky.social/xrpc/com.atproto.handle.update Authorization:"Bearer <accessJwt>" handle=your.domain.name
```

If you don't get an error, then it probably worked.

You can confirm that your request was successful if your new handle shows up in the DID:PLC server operation logs (https://plc.directory/export?after=2023-03-XXT00:00).

You can relog into Bluesky now. If you are using the beta iOS app, you will have to log in using your email address for the time being.

