+++
title = "About this Site"
render = true

[extra]
ogham = "sdflk"
+++

Most of the copy on this site was written with the assistance of a [ChatGPT](https://chat.openai.com/). Technical writings are still organical produced. (For now.)

The website is built using a static site generator called [Zola](https://www.getzola.org/), which is written in Rust 🦀.

The website's content is stored using [InterPlanetary File System](https://en.wikipedia.org/wiki/InterPlanetary_File_System) (IPFS), which is a decentralized network for storing and sharing files. This means that the website's content can be accessed directly from the IPFS network, as well as through the traditional web.
