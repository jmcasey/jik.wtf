+++
title = "What am I doing, right now?"
render = true
+++

I'm currently working on a go implementation of [Shoggoth](https://shoggoth.network/explorer/docs#what-is-shoggoth), a P2P network for ML data. Previously, I prototyped a library for the [Bluesky](bsky.app) and the underlying [@-protocol](atproto.com). You can follow my progress [here](https://gitlab.com/jmcasey/).

I currently work as a defensive cybersecurity analyst within an Incident Reponse team. Recently I have taken an interest in verifiable computing, and finite field mathematics, and [Circom](https://github.com/iden3/circom).

If our interests or projects overlap, feel free to shoot me a message: jmcsec@pm.me
